# Electron Builder Docker Image

Docker image used to build our electron application (see xivo.solutions/xucmgt> desktop application).

**Why a docker image ?**
* We need to build electron application in docker first and foremost because we need to build the Windows exe.
* The other reason to use this docker image is to not rely on NodeJs version of our builder.

**Why creating our own image when Electron ships its own docker image for building ?**
* Because at the time of doing this:
  * [Electron builder docker image wine-mono-05.18](https://hub.docker.com/layers/electronuserland/builder/wine-mono-05.18/images/sha256-a5e85af4ac5184a2274b27cb4b07014fd38c8a30754dadfbf8f93eba5bdc35e9) (with NodeJs 10,wine and mono) was borken (see [issue3630](https://github.com/electron-userland/electron-builder/pull/3630))
  * and [Electron builder docker image wine-mono](https://hub.docker.com/layers/electronuserland/builder/wine-mono/images/sha256-41476eb98388d7ba95dcdadefab1ef273dade26ec788c5e6a4aa52ad08e38ddb) (with NodeJs 12, wine and mono) was in fact built with Node Js 11 ...
* And because Electron doesn't seem to provide image with updated version of Node

## Description

This dockerfile of this image it completely built on top of [Electron builder](https://github.com/electron-userland/electron-builder/tree/master/docker).

It is based on their [base image](https://github.com/electron-userland/electron-builder/blob/master/docker/base/Dockerfile) (with the required system dependencies) and then contains the :
- Node Js installation
  - based on [electron builder node Dockerfile](https://github.com/electron-userland/electron-builder/blob/master/docker/node/Dockerfile)
  - customized with Node Js version
- Wine installation
  - based on [electron builder wine Dockerfile](https://github.com/electron-userland/electron-builder/blob/master/docker/wine/Dockerfile)
- Mono installation
  - based on [electron builder wine-mono Dockerfile](https://github.com/electron-userland/electron-builder/blob/master/docker/wine-mono/Dockerfile)
- and also Chrome and xvfb installaion
  - based on [electron builder node Dockerfile](https://github.com/electron-userland/electron-builder/blob/master/docker/wine-chrome/Dockerfile)


## Dev

To update this image you should :

- Check if their was some changes in the upstream project (see above),
- Update the Dockerfile (for example the NODE_VERSION)
- Bump the TARGET_VERSION
- Build in dev
- When ready, build in rc/prod with a tag (as for xivoxc projects)

## Build This Image

Run the [electron-builder jenkins job](http://jenkins.xivo.solutions:8080/job/electron-builder/)